import Vue from 'vue'
import Vuex from 'vuex'
import ratingStore from './rating/index.js'
import apiStore from './api/index.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    rating: ratingStore,
    api: apiStore
  }
})
