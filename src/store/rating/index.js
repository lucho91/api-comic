const ratingStore = {
  namespaced: true,
  state: () =>({
    myRating:[]
  }),
  mutations: {
    setRating(state, val){
      const comicIndex = state.myRating.findIndex((r) => r.num === val.num);
      if(comicIndex >=  0){
        state.myRating[comicIndex] = val
      }else{
        state.myRating.push(val)
      }
    }
  }

}

export default ratingStore