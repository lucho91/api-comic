import axios from "axios"
const apiStore = {
  namespaced: true,
  state: () =>({
    isLoading: false,
    canPrevious:false,
    canNext:false,
    page: 1,
    lastPage:null,
    comic:null
  }),
  mutations: {
    setCanPrevious(state, val){
      state.canPrevious = val
    },
    setCanNext(state, val){
      state.canNext = val
    },
    setLoading(state, val){
      state.isLoading = val;
    },
    addComic(state, val){
      state.comic = val;
    },
    addLastPage(state, val){
      state.lastPage = val
    },
    nextPage(state){
      state.page += 1
    },
    previousPage(state){
      state.page -= 1
    },
    setPage(state, val){
      state.page  = val
    }
  },
  actions: {
    async getLastPage(context){
      const r = await axios.get("/api/info.0.json")
      context.commit('addLastPage', r.data.num)
      context.commit('setLoading', true)
      const comic = await axios.get("/api/" + context.state.page + "/info.0.json")
      context.commit('addComic', comic.data)
      context.commit('setLoading', false)
      context.commit('setCanPrevious', context.state.page > 1)
      context.commit('setCanNext', context.state.page < context.state.lastPage)
    },
    async nextPage(context){
      context.commit('nextPage')
      context.commit('setLoading', true)
      const r = await axios.get("/api/" + context.state.page + "/info.0.json")
      context.commit('addComic', r.data)
      context.commit('setLoading', false)
      context.commit('setCanPrevious', context.state.page > 1)
      context.commit('setCanNext', context.state.page < context.state.lastPage)
    },
    async previousPage(context){
      context.commit('previousPage')
      context.commit('setLoading', true)
      const r = await axios.get("/api/" + context.state.page + "/info.0.json")
      context.commit('addComic', r.data)
      context.commit('setLoading', false)
      context.commit('setCanPrevious', context.state.page > 1)
      context.commit('setCanNext', context.state.page < context.state.lastPage)
    },
    async randomPage(context){
      const pageRandom =  Math.floor(Math.random() * (context.state.lastPage + 1))
      context.commit('setPage',pageRandom)
      context.commit('setLoading', true)
      const r = await axios.get("/api/" + context.state.page + "/info.0.json")
      context.commit('addComic', r.data)
      context.commit('setLoading', false)
      context.commit('setCanPrevious', context.state.page > 1)
      context.commit('setCanNext', context.state.page < context.state.lastPage)
    }
  },

}
export default apiStore