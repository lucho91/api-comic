import { shallowMount, createLocalVue } from '@vue/test-utils'
import CardViewComic from '@/components/CardViewComic.vue'
import axios from 'axios'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('CardViewComic.vue', () => {
    let stateApi;
    let actionsApi;
    let stateRating;
    let store;
    let modules;
    let wrapper;
    beforeEach(() => {
        stateApi = {
            isLoading: false,
            comic: {
                num:1
            },
            canPrevious:false,
            canNext: false
        }
        actionsApi = {
            getLastPage: jest.fn(),
            nextPage: jest.fn(),
            previousPage: jest.fn(),
            randomPage: jest.fn(),
        }
        stateRating = {
            myRating: []
        }
        modules = {
            api:{
                state:stateApi,
                actions:actionsApi
            },
            rating:{
                state: stateRating
            },
        }
        store = new Vuex.Store({
            modules: modules
        })
        wrapper = shallowMount(CardViewComic, {
            store, localVue,
            mocks: {
                axios: axios
            }
        });
    }); 

    it('is a Vue instance', () => {
        expect(wrapper.vm).toBeTruthy()
    })

    it('snapshot match', () => {
        expect(wrapper.html()).toMatchSnapshot()
    })

    it('mount a vue component', () => {
        const element = wrapper.findComponent(CardViewComic)
        expect(element.exists()).toBe(true)
    })
    
    it('is visible', () => {
        expect(wrapper.isVisible()).toBe(true)
    })
  })
  