### ¿Como ejecutar la aplicacion?


```
1. Clonar este repositorio
2. Acceder a la carpeta del repositorio desde la terminal
3. npm install
4. npm run serve
```

### Lanzar tests
```
npm run test:unit
```

### Pantallas

![image](./images-git/1.png)

![image](./images-git/2.png)

![image](./images-git/3.png)

### ¿Como interactuar con la aplicacion?
```

El icono refresh sirve para cargar un cómic aleatorio, el botón next sirve para cargar el cómic siguiente,
el botón previus sirve para cargar el cómic anterior y el icono ojo para ver mis cómic calificados

```

